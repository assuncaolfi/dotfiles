#!/bin/zsh

# Enable RStudio default mode

mv ~/.Rprofile2 ~/.Rprofile

rm ~/.config/rstudio/rstudio-prefs.json

ln -s ~/Repositories/dotfiles/rstudio-prefs.json ~/.config/rstudio/rstudio-prefs.json
