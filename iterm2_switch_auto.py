#!/usr/bin/env python3

import asyncio
import iterm2
import os

async def changeTheme(theme_parts, connection):
    # Themes have space-delimited attributes, one of which will be light or dark.
    if "dark" in theme_parts:
        preset = await iterm2.ColorPreset.async_get(connection, "Dracula")
        os.system("rm ~/.radian_profile")
        os.system("ln -s ~/Repositories/dotfiles/.radian_profile_dark ~/.radian_profile")
    else:
        preset = await iterm2.ColorPreset.async_get(connection, "edge-light-inventionate")
        os.system("rm ~/.radian_profile")
        os.system("ln -s ~/Repositories/dotfiles/.radian_profile_light ~/.radian_profile")

    # Update the list of all profiles and iterate over them.
    profiles=await iterm2.PartialProfile.async_query(connection)
    for partial in profiles:
        # Fetch the full profile and then set the color preset in it.
        profile = await partial.async_get_full_profile()
        await profile.async_set_color_preset(preset)

async def main(connection):
    # Set color scheme correctly at app start
    app = await iterm2.async_get_app(connection)
    parts = await app.async_get_theme()
    await changeTheme(parts, connection)


    async with iterm2.VariableMonitor(connection, iterm2.VariableScopes.APP, "effectiveTheme", None) as mon:
        while True:
            # Block until theme changes
            theme = await mon.async_get()
            parts = theme.split(" ")
            await changeTheme(parts, connection)


iterm2.run_forever(main)
