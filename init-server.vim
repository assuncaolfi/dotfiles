" __     ___            ____             __ _
" \ \   / (_)_ __ ___  / ___|___  _ __  / _(_) __ _
"  \ \ / /| | '_ ` _ \| |   / _ \| '_ \| |_| |/ _` |
"   \ V / | | | | | | | |__| (_) | | | |  _| | (_| |
"    \_/  |_|_| |_| |_|\____\___/|_| |_|_| |_|\__, |
"                                             |___/
" Author: Fabian Mundt (Inventionate)
" Repository: https://gitlab.com/Inventionate/dotfiles 
"

" ----------------------------------------------------------------------------
" OPTIONS
" ----------------------------------------------------------------------------

set autoindent                                                  " Carry over indenting from previous line
set autoread                                                    " Don't bother me when a file changes
set autowriteall                                                " Write on :next/:prev/^Z
set backspace=indent,eol,start                                  " Allow backspace beyond insertion point
set cindent                                                     " Automatic program indenting
set cinkeys-=0#                                                 " Comments don't fiddle with indenting
set cinoptions=                                                 " See :h cinoptions-values
set commentstring=\ \ #%s                                       " When folds are created, add them to this
set copyindent                                                  " Make autoindent use the same chars as prev line
set directory-=.                                                " Don't store temp files in cwd
set encoding=utf-8                                              " UTF-8 by default
scriptencoding utf-8                                            " UTF-8 by default
set expandtab                                                   " No tabs
set fileformats=unix,dos,mac                                    " Prefer Unix
set fillchars=vert:\ ,stl:\ ,stlnc:\ ,fold:-,diff:┄             " Unicode chars for diffs/folds, and rely on Colors for window borders
silent! set foldmethod=marker                                   " Use braces by default
set textwidth=100                                               " 100 is the new 80
set formatoptions=tcqn1                                         " Format text
set colorcolumn=+1                                              " Show wrap line
set hidden                                                      " Don't prompt to save hidden windows until exit
set history=1000                                                " How many lines of history to save
set hlsearch                                                    " Hilight searching
set ignorecase                                                  " Case insensitive
set incsearch                                                   " Search as you type
set infercase                                                   " Completion recognizes capitalization
set laststatus=2                                                " Always show the status bar
set linebreak                                                   " Break long lines by word, not char
set list                                                        " Show whitespace as special chars - see listchars
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·       " Unicode characters for various things
set matchtime=2                                                 " Tenths of second to hilight matching paren
set modelines=5                                                 " How many lines of head & tail to look for ml's
set nobackup                                                    " No backups left after done editing
set nonumber                                                    " No line numbers to start
set signcolumn=yes                                              " Always show sign column
set visualbell t_vb=                                            " No flashing or beeping at all
set nowritebackup                                               " No backups made while editing
set printoptions=paper:A4                                       " A4 paper
set ruler                                                       " Show row/col and percentage
set scroll=4                                                    " Number of lines to scroll with ^U/^D
set scrolloff=15                                                " Keep cursor away from this many chars top/bot
set sessionoptions-=options                                     " Don't save runtimepath in Vim session (see tpope/vim-pathogen docs)
set shiftround                                                  " Shift to certain columns, not just n spaces
set shiftwidth=4                                                " Number of spaces to shift for autoindent or >,<
set shortmess+=A                                                " Don't bother me when a swapfile exists
set shortmess+=c                                                " Hide Omnicomplete messages
set showbreak=                                                  " Show for lines that have been wrapped, like Emacs
set showmatch                                                   " showmatch Hilight matching braces/parens/etc.
set noshowmode                                                  " Turn off show mode (showed by lightline)
set sidescrolloff=3                                             " Keep cursor away from this many chars left/right
set smartcase                                                   " Lets you search for ALL CAPS
set softtabstop=4                                               " Spaces 'feel' like tabs
set suffixes+=.pyc                                              " Ignore these files when tab-completing
set tabstop=4                                                   " The One True Tab
set notitle                                                     " Don't set the title of the Vim window
set wildmenu                                                    " Show possible completions on command line
set wildmode=list:longest,full                                  " List all options and complete
set wildignore=*.class,*.o,*~,*.pyc,.git,.DS_Store              " Ignore certain files in tab-completion
set splitbelow                                                  " Make splits defaut to below...
set splitright                                                  " And to the right
set showtabline=0                                               " Always hide tabline
filetype plugin indent on                                       " Essential for filetype plugins
set backupcopy=yes                                              " Make sure the original file is overwritten on save
set undofile                                                    " Enable persistent undo
set undodir=$HOME/.config/nvim/undo                             " Undo file dir
set nomodeline                                                  " Turn off modeline support.

" ----------------------------------------------------------------------------
" PLUGIN SETTINGS
" ----------------------------------------------------------------------------

so ~/.config/nvim/plugins-server.vim

" For any plugins that use this, make their keymappings use comma
let mapleader = ','
let maplocalleader = ';'

" FZF (replaces Ctrl-P, FuzzyFinder and Command-T)
command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --column --line-number --no-heading --color=always --smart-case '.shellescape(<q-args>), 1,
      \   <bang>0 ? fzf#vim#with_preview('up:60%')
      \           : fzf#vim#with_preview('right:50%:hidden', '?'),
      \   <bang>0)

" ALE
let g:ale_sign_warning = '●'
let g:ale_sign_error = '●'
" ALE Linters
let g:ale_linters = {
            \ 'php': ['phpstan'],
            \ 'javascript': ['eslint'],
            \ 'scss': ['stylelint'],
            \ 'vue': ['vls'],
            \ 'vim': ['vint'],
            \}
" ALE Fixers
let g:ale_fixers = {
            \ 'javascript': ['prettier'],
            \ 'vue': ['prettier'],
            \ 'typescript': ['prettier'],
            \ 'php': ['prettier'],
            \ 'vim': ['vint'],
            \}

" Lightline
let g:lightline#ale#indicator_checking = "\uf110"
let g:lightline#ale#indicator_warnings = "\uf071 "
let g:lightline#ale#indicator_errors = "\uf00d "
let g:lightline#ale#indicator_ok = "\uf00c"
let g:lightline = {
      \ 'colorscheme': 'dracula',
      \ 'active': {
      \   'left': [
      \     [ 'mode', 'paste' ], 
      \     [ 'gitbranch' ], 
      \     [ 'readonly', 'filename' ]
      \   ],
      \   'right': [
      \     [ 'lineinfo' ], 
      \     [ 'percent' ], 
      \     [ 'filetype'],
      \     [ 'linter_errors', 'linter_warnings' ]
      \   ]
      \ },
      \ 'component_expand': {
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \ },
      \ 'component_type': {
      \   'linter_warnings': 'warning',
      \   'linter_errors': 'error',
      \ },
      \ 'component_function': {
      \   'filename': 'LightlineFilename',
      \   'gitbranch': 'MyFugitive'
      \ },
      \ }

function! LightlineFilename()
  let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
  let modified = &modified ? ' +' : ''
  return filename . modified
endfunction

function! MyFugitive()
  if exists('*fugitive#head') && winwidth('.') > 75
    let bmark = "\uf126 "
    let branch = fugitive#head()
    return strlen(branch) ? bmark . branch : ''
  endif
  return ''
endfunction

" IndentLine
let g:indentLine_char = '│'

" Signify
let g:signify_sign_add = '┃'
let g:signify_sign_change = '┃'
let g:signify_sign_delete = '‣'

" Startify
let g:startify_files_number = 5
let g:startify_change_to_dir = 1
let g:startify_session_dir = $HOME . '/.config/nvim/session'
let g:startify_custom_header = []
let g:startify_lists = [
      \ { 'type': 'files',     'header': ['   MRU']       },
      \ { 'type': 'sessions',  'header': ['   Sessions']  },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks'] },
      \]

" ----------------------------------------------------------------------------
" AUTOCOMMANDS
" ----------------------------------------------------------------------------

" ALE
" Project specific linters
function! UseLarastan()
  let b:ale_php_phpstan_executable = './vendor/bin/phpstan'
endfunction

augroup ProjectSpecificLinters
  autocmd!
  autocmd BufReadPre,FileReadPre ~/Repositories/lemas-webapp/*.php call UseLarastan() 
augroup END

" ----------------------------------------------------------------------------
" COLORS
" ----------------------------------------------------------------------------

" Colour theme
set background=dark
color dracula

" Use 24bit colours.
set termguicolors

" We'll fake a custom left padding for each window.
set foldcolumn=1

" Better search colour
highlight Search guibg=#50FA7B guifg=#000000

" Distinguishing terminal mode cursor
highlight! link TermCursor Cursor
highlight! TermCursorNC guibg=#FF5555 guifg=#FFFFFF ctermbg=1 ctermfg=15

" Signify
highlight SignifySignAdd cterm=bold ctermfg=84 guifg=#50FA7B
highlight SignifySignDelete cterm=bold ctermfg=203 guifg=#FF5555
highlight SignifySignChange cterm=bold ctermfg=215 guifg=#FFB86C

" ----------------------------------------------------------------------------
" KEY MAPS
" ----------------------------------------------------------------------------

" We'll set simpler mappings to switch between splits.
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

" Make it easy to edit the Vimrc file.
nmap <Leader>ev :tabedit $MYVIMRC<cr>

" Turn off linewise keys. Normally, the `j' and `k' keys move the cursor down one entire line. with
" line wrapping on, this can cause the cursor to actually skip a few lines on the screen because
" it's moving from line N to line N+1 in the file. I want this to act more visually -- I want `down'
" to mean the next line on the screen
nmap j gj
nmap k gk

" FZF
nmap <silent> <leader>f :Files<CR>
nmap <silent> <leader>h :History<CR>
nmap <silent> <leader>t :Tags<CR>
nmap <silent> <leader>b :Buffers<CR>
nmap <silent> <leader>s :Rg<CR>

" ALE
nmap <silent> <W <Plug>(ale_first)
nmap <silent> <w <Plug>(ale_previous)
nmap <silent> >w <Plug>(ale_next)
nmap <silent> >W <Plug>(ale_last)

" Exit Terminal mode by ESC
tnoremap <Esc> <C-\><C-n>

" Exit Shell program 
tnoremap <C-v><Esc> <Esc>

" Improve manuel omni complete
inoremap <leader>, <C-x><C-o>

" Easy align
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" Incremental search improvements
map n <Plug>(is-nohl)<Plug>(anzu-n-with-echo)
map N <Plug>(is-nohl)<Plug>(anzu-N-with-echo)
