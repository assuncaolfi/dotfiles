setl number
set textwidth=100
set formatoptions+=t
set formatoptions-=c
set wrapmargin=2
set signcolumn=no
