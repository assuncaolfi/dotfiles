#!/bin/zsh

brew upgrade
mas upgrade 
brew cu --all -y
composer global upgrade
pecl upgrade msgpack
gem update
pip3 install neovim --upgrade
pip3 install vim-vint --upgrade
rustup update
npm update -g
sudo tlmgr update --self --all
sudo R -e "update.packages(ask = FALSE, repos = 'https://cloud.r-project.org/', Ncpus = 5)"
