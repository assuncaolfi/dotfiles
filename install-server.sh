#!/bin/sh

# Install dependencies

if [ ! -f /usr/local/extra_homestead_software_installed ]; then
    sudo apt install -y language-pack-de
    sudo apt-get install -y zsh
    sudo chsh -s $(which zsh) $(whoami)
    git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    curl -fsSL https://starship.rs/install.sh | bash
    sudo apt-get install -y python-dev python-pip python3-dev python3-pip
    pip install --upgrade pip
    curl -LO https://github.com/BurntSushi/ripgrep/releases/download/0.10.0/ripgrep_0.10.0_amd64.deb
    sudo dpkg -i ripgrep_0.10.0_amd64.deb
    sudo apt-get install -y software-properties-common
    sudo apt-add-repository -y ppa:neovim-ppa/stable
    sudo apt-get update
    sudo apt-get install -y neovim
    sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
    sudo update-alternatives --config vi
    sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
    sudo update-alternatives --config vim
    sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
    sudo update-alternatives --config editorupdate-alternatives
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    sudo apt-get install -y mycli
    # Install Neovim packages
    pip3 install neovim
    yarn global add neovim
    sudo apt-get install tree

    # Create softlinks
    mkdir ~/.config/nvim
    ln -s ~/repositories/dotfiles/init-server.vim ~/.config/nvim/init.vim
    ln -s ~/repositories/dotfiles/plugins-server.vim ~/.config/nvim/plugins-server.vim
    rm ~/.zshrc
    ln -s ~/repositories/dotfiles/.zshrc ~/.zshrc

    # Prefs
    git config --global user.name "Fabian Mundt"
    git config --global user.email "f.mundt@inventionate.de"

    # Add folder for homestead
    sudo touch /usr/local/extra_homestead_software_installed
fi
