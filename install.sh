#!/bin/sh

# Install TeX
brew install --cask basictex
tlmgr install luatex85
tlmgr install preview
tlmgr install xcolor
tlmgr install pgf
tlmgr install placeins
tlmgr install latex-tools
tlmgr install afterpage
tlmgr install selnolig
tlmgr install lualatex-math
tlmgr install makecell
tlmgr install environ
tlmgr install threeparttablex
tlmgr install threeparttable
tlmgr install wrapfig
tlmgr install multirow
tlmgr install pdfcrop
tlmgr install framed
tlmgr install float
tlmgr install microtype
tlmgr install xindy
tlmgr install texindy
tlmgr install biber
tlmgr install rotfloat
tlmgr install enumitem
tlmgr install mathastext
tlmgr install blindtext
tlmgr install qrcode
tlmgr install varwidth
tlmgr install tabu
tlmgr install nextpage
tlmgr install epigraph
tlmgr install changepage
tlmgr install mcaption
tlmgr install cleveref
tlmgr install idxlayout
tlmgr install imkakeidx
tlmgr install ifmtarg
tlmgr install biblatex-apa
tlmgr install biblatex
tlmgr install xifthen
tlmgr install csquotes
tlmgr install luacode
tlmgr install xpatch
tlmgr install pdfescape
tlmgr install alphalph

# Install software
brew install --cask bibdesk
brew install --cask skim
brew install --cask handbrake
brew install --cask iterm2
brew install --cask rstudio
brew install --cask nextcloud
brew install --cask homebrew/cask-versions/firefox-developer-edition
brew install --cask qlmarkdown
brew install --cask qlstephen
brew install --cask quicklook-csv
brew install --cask zulu
brew install --cask xquartz
brew install --cask vimr
brew install --cask iina
brew install --cask r
brew install --cask sequel-ace
brew install --cask quarto
brew install php
brew install deployer
brew install youtube-dl
brew install ffmpeg
brew install git
brew install autossh
brew install tree
brew install fd
brew install optipng
brew install languagetool
brew install luarocks
brew install mas
brew install grex
brew tap buo/cask-upgrade
brew update 

# Install dependencies
brew install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
brew install exa
brew install python3
brew install lua
brew install gpg2
brew install pinentry-mac
brew install ripgrep
brew install pandoc
brew install pandoc-citeproc
brew install pandoc-crossref
brew install llvm
brew install rbenv ruby-build
rbenv install 3.0.2
rbenv global 3.0.2
brew install node
brew install neovim
brew install starship
brew install saulpw/vd/visidata
brew install trash-cli
brew tap caskroom/fonts
brew install docker-compose
brew install --cask docker
brew install --cask lulu
brew install --cask blockblock
brew install --cask ransomwhere
brew install --cask knockknock
brew install --cask taskexplorer
brew install --cask font-firacode-nerd-font
brew install --cask font-vollkorn
brew install --cask font-fira-sans
brew install --cask font-fontawesome
brew install --cask font-roboto
brew install --cask font-roboto-slab
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git config --global core.excludesfile ~/Repositories/dotfiles/.gitignore_global
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -y

# Install Neovim packages
pecl install msgpack
pip3 install pynvim
pip3 install neovim-remote
pip3 install -U radian
gem install neovim
npm install -g neovim

# Install Radian Dracula theme
cd ~
git clone https://github.com/dracula/pygments.git
cp pygments/dracula.py /usr/local/lib/python3.9/site-packages/pygments/styles/dracula.py
pygmentize -S dracula -f html > dracula.css
rm -rf ~/pygments

# LSP Servers
R -e "install.packages('languageserver', repos='http://cran.us.r-project.org')"

# Create softlinks
ln -s ~/Library/Mobile\ Documents/com\~apple\~CloudDocs ~/iCloud
ln -s ~/Library/Mobile\ Documents/com\~apple\~CloudDocs/Dissertation ~/Dissertation
mkdir ~/.config/nvim
ln -s ~/repositories/dotfiles/init.vim ~/.config/nvim/init.vim
ln -s ~/repositories/dotfiles/plugins.vim ~/.config/nvim/plugins.vim
ln -s ~/repositories/dotfiles/ftplugin ~/.config/nvim/ftplugin
ln -s ~/repositories/dotfiles/ftdetect ~/.config/nvim/ftdetect
ln -s ~/Repositories/dotfiles/after ~/.config/nvim/after
rm ~/.zshrc
ln -s ~/Repositories/dotfiles/.zshrc ~/.zshrc
ln -s ~/Repositories/dotfiles/starship.toml ~/.config/starship.toml
ln -s ~/iCloud/Vim/spell ~/.config/nvim/spell
ln -s ~/Repositories/dotfiles/.Rprofile ~/.Rprofile
ln -s ~/Repositories/dotfiles/rstudio-prefs.json ~/.config/rstudio/rstudio-prefs.json
ln -s ~/Repositories/dotfiles/TypeInfo.plist ~/Library/Application\ Support/BibDesk/TypeInfo.plist
ln -s ~/Repositories/dotfiles/nvim/lua ~/.config/nvim/lua
ln -s ~/Repositories/dotfiles/.radian_profile ~/.radian_profile
ln -s ~/Repositories/dotfiles/.lintr ~/.lintr
ln -s ~/Repositories/dotfiles/Neovim.app /Applications/Neovim.app
ln -s ~/Repositories/dotfiles/gpg-agent.conf ~/.gnupg/gpg-agent.conf
mkdir ~/Library/ApplicationSupport/iTerm2/Scripts/AutoLaunch
ln -s ~/Repositories/dotfiles/iterm2_switch_auto.py ~/Library/ApplicationSupport/iTerm2/Scripts/AutoLaunch/switch_auto.py

# Prefs
git config --global user.name "Fabian Mundt"
git config --global user.email "f.mundt@inventionate.de"
git config --global commit.gpgsign true

# R Packages
R -e "install.packages('lintr', repos='http://cran.us.r-project.org')"
R -e "install.packages('tidyverse', repos='http://cran.us.r-project.org')"
R -e "install.packages('gert', repos='http://cran.us.r-project.org')"
