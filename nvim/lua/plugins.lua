local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
    execute 'packadd packer.nvim'
end

return require('packer').startup {
    function(use)
        -- Packer can manage itself
        use 'wbthomason/packer.nvim'
        -- CursorHold fix
        use 'antoinemadec/FixCursorHold.nvim'
        -- Colour theme
        use {
            'dracula/vim',
            as = 'dracula'
        }
        -- Zen mode theme
        use {
            'YorickPeterse/vim-paper',
            as = 'paper'
        }
        -- Light mode theme
        use 'sainnhe/edge'
        -- Git integration
        use 'tpope/vim-fugitive'
        -- Terminal commands
        use 'tpope/vim-eunuch'
        -- Search and replace improvement
        use 'tpope/vim-abolish'
        -- Improved comments
        use {
            'b3nj5m1n/kommentary',
            config = [[
            require('kommentary.config').configure_language("php", {
            single_line_comment_string = "//",
            multi_line_comment_strings = {"/*", "*/"},
            })]]
        }
        -- Peeking the buffer while entering line number
        use {
            'nacro90/numb.nvim',
            config = function()
                require('numb').setup()
            end
        }
        -- Which key
        use {
            "folke/which-key.nvim",
            config = function()
                require("which-key").setup()
            end
        }
        -- Git status in gutter
        use {
            'lewis6991/gitsigns.nvim',
            requires = {{'nvim-lua/plenary.nvim'}},
            config = [[require('config.gitsigns')]]
        }
        -- Startscreen
        use 'glepnir/dashboard-nvim'
        -- Laravel Blade Syntax
        use {'jwalton512/vim-blade'}
        -- Improved search
        use {
            'kevinhwang91/nvim-hlslens',
            config = function()
                require('hlslens').setup({
                    calm_down = true,
                })
            end
        }
        -- Status bar
        use {
            'nvim-lualine/lualine.nvim',
            config = [[require('config.lualine')]]
        }
        -- Better Omnicomplete
        use 'lifepillar/vim-mucomplete'
        -- Support editorconfig
        use 'editorconfig/editorconfig-vim'
        -- Improved alignments
        use 'junegunn/vim-easy-align'
        -- Dev icon support
        use 'kyazdani42/nvim-web-devicons'
        -- Fast navigation
        use 'ggandor/lightspeed.nvim'
        -- Close all hidden buffers
        use 'schickling/vim-bufonly'
        -- Improved buffer close
        use 'ojroques/nvim-bufdel'
        -- Fuzzy search
        use {
            'liuchengxu/vim-clap',
            run = ':Clap install-binary!'
        }
        -- Language Server support
        use {
            'neovim/nvim-lspconfig',
            requires = {
                -- Info about Code Actions
                {'kosayoda/nvim-lightbulb'},
            },
            config = [[require('config.lsp')]]
        }
        -- Auto pairs
        use 'tmsvg/pear-tree'
        -- Highlight yank
        use 'machakann/vim-highlightedyank'
        -- Colour output
        use {
            'norcalli/nvim-colorizer.lua',
            config = function() require('colorizer').setup() end
        }
        use {
            "folke/todo-comments.nvim",
            requires = "nvim-lua/plenary.nvim",
            config = function()
                require("todo-comments").setup {}
            end
        }
        -- Floating terminal
        use 'voldikss/vim-floaterm'
        -- Mark line indent
        use 'lukas-reineke/indent-blankline.nvim'
        -- R integration
        use {
            'jalvesaq/Nvim-R',
            branch = 'master',
            ft = {'r', 'rmd', 'rnoweb', 'rhelp', 'rrst'}
        }
        -- Documentation
        use {
            'kkoomen/vim-doge',
            run = ':call doge#install()'
        }
        -- Lua Syntax
        use {'euclidianAce/BetterLua.vim'}
        -- Undotree
        use 'simnalamburt/vim-mundo'
        -- Treesitter
        use {
            'nvim-treesitter/nvim-treesitter',
            run = ':TSUpdate',
            requires = {
                -- Treesitter context
                {'romgrk/nvim-treesitter-context'},
                -- Playground
                {'nvim-treesitter/playground'}
            },
            config = [[require('config.treesitter')]]
        }
        -- Automatic project wd switch
        use 'airblade/vim-rooter'
        -- Smooth scrolling
        use 'psliwka/vim-smoothie'
        -- Pandoc Markdown plugin
        use {
            'vim-pandoc/vim-pandoc',
            requires = {
                -- Pandoc syntax highlighter
                {'inventionate/vim-pandoc-syntax'},
                -- Quarto syntax support
                {'quarto-dev/quarto-vim'},
                -- Pandoc compatibility mode
                {'vim-pandoc/vim-pandoc-after'}
            }
        }
        -- Test suite
        use { 'rcarriga/vim-ultest',
            requires = {'janko/vim-test'},
            run = ':UpdateRemotePlugins'
        }
        -- Zen mode
        use {
            "folke/zen-mode.nvim",
            config = [[require('config.zen-mode')]],
            requires = {
                -- Zen focus addon
                {'junegunn/limelight.vim'}
            },
        }
        -- LanguageTool
        use {
            'rhysd/vim-grammarous',
            ft = {'rmd', 'text', 'pandoc', 'tmd', 'markdown'}
        }
        -- Edit tables in style
        use {
            'dhruvasagar/vim-table-mode',
            ft = {'rmd', 'text', 'pandoc', 'tmd', 'markdown'}
        }
        -- Thesaurus
        use {
            'ron89/thesaurus_query.vim',
            ft = {'rmd', 'text', 'pandoc', 'tmd', 'markdown'}
        }
    end,
    config = {
        display = {
            open_fn = function()
                return require('packer.util').float({ border = 'none' })
            end
        }
    }
}
