require('gitsigns').setup {
signs = {
    add          = {hl = 'DiffAdd'   , text = '│'},
    change       = {hl = 'DiffChange', text = '│'},
    delete       = {hl = 'DiffDelete', text = '►', show_count=true},
    topdelete    = {hl = 'DiffDelete', text = '►', show_count=true},
    changedelete = {hl = 'DiffChange', text = '►', show_count=true},
  },
  count_chars = {
    [1]   = '₁',
    [2]   = '₂',
    [3]   = '₃',
    [4]   = '₄',
    [5]   = '₅',
    [6]   = '₆',
    [7]   = '₇',
    [8]   = '₈',
    [9]   = '₉',
    ['+'] = '₊',
  }
}

