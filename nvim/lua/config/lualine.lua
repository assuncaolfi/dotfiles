local dynamic_theme = 'onelight'

if string.find(vim.fn.system("defaults read -g AppleInterfaceStyle"), 'Dark') then
    dynamic_theme = 'dracula'
    else
end

require('lualine').setup {
    options = {
        theme = dynamic_theme,
        section_separators = {''},
        component_separators = {'|', '|'},
        disabled_filetypes = {},
        icons_enabled = true,
    },
    sections = {
        lualine_a = { {'mode', upper = true} },
        lualine_b = { {'branch', icon = ''} },
        lualine_c = { {'filename', file_status = true} },
        lualine_x = { {'diagnostics',
            sources = {'nvim_diagnostic'},
            color_error = '#ff5555',
            color_warn = '#ffb86c',
            color_info = '#8be9fd',
            symbols = {error = ' ', warn = ' ', info = ' '}
        }, 'encoding', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location'  },
    },
}
