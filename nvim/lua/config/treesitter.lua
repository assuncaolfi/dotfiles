require'nvim-treesitter.configs'.setup {
    ensure_installed = {
        "bash",
        "css",
        "html",
        "javascript",
        "json",
        "lua",
        "php",
        "r",
        "regex",
        "vue"
    },
    highlight = {
        enable = true
    },
    indent = {
        enable = true
    }
}

local ft_to_parser = require('nvim-treesitter.parsers').filetype_to_parsername
ft_to_parser.rmd = 'markdown'

-- Handle Blade syntax in Blade files
if packer_plugins["nvim-treesitter"] and packer_plugins["vim-blade"] then
    vim.api.nvim_command [[autocmd FileType blade :TSBufDisable highlight]]
end
