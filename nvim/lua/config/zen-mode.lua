require("zen-mode").setup {
    window = {
        backdrop = 1,
        width = 110,
        height = 0.8,
        options = {
            signcolumn = "no",
            number = false,
        },
    },
    plugins = {
        gitsigns = {
            enabled = false
        },
        options = {
            enabled = true,
            ruler = false,
            showcmd = false,
        },
    },
    on_open = function(win)
        vim.api.nvim_set_option('scrolloff', 9999)
        vim.cmd('Limelight')
        vim.cmd('color paper')
    end,
    on_close = function()
        vim.api.nvim_set_option('scrolloff', 15)
        vim.cmd('Limelight!')
        if string.find(vim.fn.system("defaults read -g AppleInterfaceStyle"), 'Dark') then
            vim.cmd('color dracula')
        else
            vim.cmd('color edge')
            vim.cmd('set background=light')
        end
        vim.cmd('syntax on')
    end,
}
