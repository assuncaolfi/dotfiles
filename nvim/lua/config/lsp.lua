local lspconfig = require('lspconfig')
local configs = require('lspconfig.configs')

-- LSP omnifunc sync version for MUComplete chains
local omnifunc_cache
function _G.omnifunc_sync(findstart, base)
  local pos = vim.api.nvim_win_get_cursor(0)
  local line = vim.api.nvim_get_current_line()

  if findstart == 1 then
    -- Cache state of cursor line and position due to the fact that it will
    -- change at the second call to this function (with `findstart = 0`). See:
    -- https://github.com/vim/vim/issues/8510.
    -- This is needed because request to LSP server is made on second call.
    -- If not done, server's completion mechanics will operate on different
    -- document and position.
    omnifunc_cache = {pos = pos, line = line}

    -- On first call return column of completion start
    local line_to_cursor = line:sub(1, pos[2])
    return vim.fn.match(line_to_cursor, '\\k*$')
  end

  -- Restore cursor line and position to the state of first call
  vim.api.nvim_set_current_line(omnifunc_cache.line)
  vim.api.nvim_win_set_cursor(0, omnifunc_cache.pos)

  -- Make request
  local bufnr = vim.api.nvim_get_current_buf()
  local params = vim.lsp.util.make_position_params()
  local result = vim.lsp.buf_request_sync(bufnr, 'textDocument/completion', params, 2000)
  if not result then return {} end

  -- Transform request answer to list of completion matches
  local items = {}
  for _, item in pairs(result) do
    if not item.err then
      local matches = vim.lsp.util.text_document_completion_list_to_complete_items(item.result, base)
      vim.list_extend(items, matches)
    end
  end

  -- Restore back cursor line and position to the state of this call's start
  -- (avoids outcomes of Vim's internal line postprocessing)
  vim.api.nvim_set_current_line(line)
  vim.api.nvim_win_set_cursor(0, pos)

  return items
end

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
    -- Enable omnifunc in general
    -- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
    buf_set_option('omnifunc', 'v:lua.omnifunc_sync')
    -- Diagnostic Signs
    vim.fn.sign_define("DiagnosticSignError",
        {text = "●", texthl = "DiagnosticSignError", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignWarn",
        {text = "●", texthl = "DiagnosticSignWarning", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignInfo",
        {text = "●", texthl = "DiagnosticSignInormation", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignHint",
        {text = "●", texthl = "DiagnosticSignHint", linehl = "", numhl = ""})
    -- Mappings
    local opts = { noremap=true, silent=true }
    buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<leader>r', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float({scope = "cursor"})<CR>', opts)
    buf_set_keymap('n', '<leader>n', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
    buf_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
    -- Set some keybinds conditional on server capabilities
    if client.resolved_capabilities.document_formatting then
        buf_set_keymap("n", "ff", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
    end
    if client.resolved_capabilities.document_range_formatting then
        buf_set_keymap("v", "ff", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
    end
    -- Set autocommands conditional on server_capabilities
    if client.resolved_capabilities.document_highlight then
        vim.api.nvim_exec([[
        augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
        augroup END
            ]], false)
    end
end

lspconfig.html.setup {
    on_attach = on_attach,
    filetypes = { "html", "blade" }
}

lspconfig.intelephense.setup {
    on_attach = on_attach,
    init_options = {
        licenceKey = "/Users/fabianmundt/iCloud/Documents/Sicherheitscodes/intelephense.txt";
      }
}

lspconfig.cssls.setup {
    on_attach = on_attach,
    settings = {
        css = { validate = false },
        scss = { validate = false }
    }
}

lspconfig.r_language_server.setup {
    on_attach = on_attach,
}

lspconfig.vimls.setup {
    on_attach = on_attach,
}

lspconfig.texlab.setup {
    on_attach = on_attach,
}

lspconfig.jsonls.setup {
    on_attach = on_attach,
}

lspconfig.ltex.setup {
    on_attach = on_attach,
    filetypes = { "bib", "markdown", "org", "plaintex", "rst", "rnoweb", "tex", "rmd" },
    settings = {
        ltex = {
            language = 'de-DE',
            additionalRules = {
                enablePickyRules = true,
                motherTongue = 'de-DE',
                languageModel = '/Users/fabianmundt/Repositories/proofreading/n-gram-index',
            },
            disabledRules = { ['de-DE'] = { 
                'UNPAIRED_BRACKETS', 
                'FALSCHES_ANFUEHRUNGSZEICHEN',
                'BISSTRICH',
                'ABKUERZUNG_LEERZEICHEN',
                'TYPOGRAFISCHE_ANFUEHRUNGSZEICHEN',
                'AUSLASSUNGSPUNKTE',
                } 
            },
            dictionary = {
                ['de-DE'] = { 'Phänomenotechnik', 'rhizomorph', 'Zeit–Raum-Konstellationen', 'rhizomorphes', '.side',
                'korresponenzanalytisch', 'korrespondenzanalytische', 'sozialtopologische'},
            },
            --[[ hiddenFalsePositives = {
                ['de-DE'] = {
                    {['rule'] = 'GERMAN_SPELLER_RULE', ['sentence'] = '@Pfaff-Czarnecka:2016ui'}
                }
            } ]]
        }
    }
}

-- Setup LSP Diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        virtual_text = {
            spacing = 4,
            prefix = ' '
        },
        signs = true,
        update_in_insert = false
    }
)

-- Load settings after LSP is enabled
if packer_plugins["nvim-lspconfig"] then
    -- Handle PHP Intelephense completion bug
    vim.api.nvim_command [[autocmd FileType php setlocal iskeyword+=$]]
    -- Enable Lightbulb
    vim.api.nvim_command [[autocmd CursorHold,CursorHoldI *.php lua require'nvim-lightbulb'.update_lightbulb()]]
    vim.fn.sign_define("LightBulbSign", {text = "",  texthl = "LspLightBulbSign", linehl = "", numhl = ""})
end
