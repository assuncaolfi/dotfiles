# Do not copy the whole configuration, just specify what you need!
# see https://help.farbox.com/pygments.html
# for a list of supported color schemes, default scheme is "native"
# Dracula Theme have to be installed manually for Pygments:
# https://draculatheme.com/pygments/
options(radian.color_scheme = "colorful")

# either  `"emacs"` (default) or `"vi"`.
options(radian.editing_mode = "vi")

# enable the [prompt_toolkit](https://python-prompt-toolkit.readthedocs.io/en/master/index.html) [`auto_suggest` feature](https://python-prompt-toolkit.readthedocs.io/en/master/pages/asking_for_input.html#auto-suggeston)
# this option is experimental and is known to break python prompt, use it with caution
options(radian.auto_suggest = FALSE)

# highlight matching bracket
options(radian.highlight_matching_bracket = TRUE)

# pop up completion while typing
options(radian.complete_while_typing = FALSE)
# the minimum length of prefix to trigger auto completions
options(radian.completion_prefix_length = 2)
# timeout in seconds to cancel completion if it takes too long
# set it to 0 to disable it
options(radian.completion_timeout = 0.05)

# custom prompt for different modes
options(radian.prompt = "\033[0;34mR❯\033[0m ")
options(radian.shell_prompt = "\033[0;31m#!❯\033[0m ")
options(radian.browse_prompt = "\033[0;33mBrowse[{}]❯\033[0m ")

# show vi mode state when radian.editing_mode is `vi`
options(radian.show_vi_mode_prompt = FALSE)
options(radian.auto_complete_function_parentheses = TRUE)
options(radian.auto_complete_selected_option_on_tab = TRUE)
options(radian.auto_complete_top_option_on_enter = TRUE)
options(radian.auto_complete_top_option_on_tab = TRUE)
options(radian.auto_complete_only_option_on_tab = TRUE)

# stderr color format
options(radian.stderr_format = "\033[0;31m{}\033[0m")

# allows user defined shortcuts, these keys should be escaped when send through the terminal.
# In the following example, `esc` + `-` sends `<-` and `esc` + `m` sends `%>%`.
# Note that in some terminals, you could mark `alt` as `escape` so you could use `alt` + `-` instead.
options(radian.escape_key_map = list(
    list(key = "-", value = " <- "),
    list(key = "m", value = " %>% ")
))
