" ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗
" ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║
" ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║
" ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║
" ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║
" ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝
"
" Author: Fabian Mundt (Inventionate)
" Repository: https://gitlab.com/Inventionate/dotfiles
"
" ----------------------------------------------------------------------------
" OPTIONS
" ----------------------------------------------------------------------------

" Carry over indenting from previous line
set autoindent
" Use smart indention
set smartindent
" Allow backspace beyond insertion point
set backspace=indent,eol,start
" Automatic program indenting
set cindent
" Comments don't fiddle with indenting
set cinkeys-=0#
" See :h cinoptions-values
set cinoptions=
" When folds are created, add them to this
set commentstring=\ \ #%s
" UTF-8 by default
set encoding=utf-8
" UTF-8 by default
scriptencoding utf-8
" No tabs
set expandtab
" Prefer Unix
set fileformats=unix,dos,mac
" Unicode chars for diffs/folds, and rely on Colors for window borders
set fillchars=vert:\ ,stl:\ ,stlnc:\ ,fold:-,diff:┄
" Use braces by default
set foldmethod=marker
" 120 is the new 80
set textwidth=120
" Format text
set formatoptions=tcqn1
" Show wrap line
set colorcolumn=+1
" How many lines of history to save
set history=1000
" Hilight searching
set hlsearch
" Case insensitive
set ignorecase
" live search substitution
set inccommand=nosplit
" Search as you type
set incsearch
" Completion recognizes capitalization
set infercase
" Always show the status bar
set laststatus=2
" Break long lines by word, not char
set linebreak
" Show whitespace as special chars - see listchars
set list
" Unicode characters for various things
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:¶,trail:·
" Tenths of second to hilight matching paren
set matchtime=2
" How many lines of head & tail to look for ml's
set modelines=5
" No line numbers to start
set number
" Signs in number column
set signcolumn=number
" No flashing or beeping at all
set visualbell t_vb=
" A4 paper
set printoptions=paper:A4
" Hide row/col and percentage
set noruler
" Hise command line infos
set noshowcmd
" Number of lines to scroll with ^U/^D
set scroll=4
" Keep cursor away from this many chars top/bot
set scrolloff=15
" Enable mouse support
set mouse=a
" Don't save runtimepath in Vim session (see tpope/vim-pathogen docs)
set sessionoptions-=options
" Shift to certain columns, not just n spaces
set shiftround
" Number of spaces to shift for autoindent or >,<
set shiftwidth=4
" Hide Omnicomplete messages
set shortmess+=c
" Show for lines that have been wrapped, like Emacs
set showbreak=
" Showmatch Hilight matching braces/parens/etc.
set showmatch
" Turn off show mode (showed by lightline)
set noshowmode
" Keep cursor away from this many chars left/right
set sidescrolloff=3
" Lets you search for ALL CAPS
set smartcase
" Spaces 'feel' like tabs
set softtabstop=4
" Ignore these files when tab-completing
set suffixes+=.pyc
" The One True Tab
set tabstop=4
" Don't set the title of the Vim window
set notitle
" Disable conceal
set conceallevel=0
" Disable omnicompletion preview
set completeopt=menuone,noselect,noinsert
" Ignore certain files in tab-completion
set wildignore=*.class,*.o,*~,*.pyc,.git,.DS_Store
" Make splits defaut to below ...
set splitbelow
" ... and to the right
set splitright
" Always hide tabline
set showtabline=0
" Essential for filetype plugins
filetype plugin indent on
" Make sure the original file is overwritten on save
set backupcopy=yes
" Backup file dir
set backupdir=$HOME/iCloud/Vim/backup
" Swap file dir
set directory=$HOME/iCloud/Vim/swap
" Enable persistent undo
set undofile
" Undo file dir
set undodir=$HOME/iCloud/Vim/undo
" Turn off modeline support
set nomodeline
" Set update time
set updatetime=1000
" Autosave
set autowriteall

" ----------------------------------------------------------------------------
" PLUGIN SETTINGS
" ----------------------------------------------------------------------------

" Load plugins
" so ~/.config/nvim/plugins.vim
lua require('plugins')

" For any plugins that use this, make their keymappings use comma
let mapleader = ','
let maplocalleader = ';'

" MUComplete
let g:mucomplete#always_use_completeopt = 1
let g:mucomplete#chains = {}
let g:mucomplete#chains = {
            \ 'default' : ['omni', 'path', 'c-n'],
            \ }
let g:mucomplete#chains['rmd'] = {
            \ 'default' : ['user', 'path'],
            \ 'rmdrChunk' : ['omni', 'path'],
            \ }

" Clap
let g:clap_selected_sign = { 'text': '->', 'texthl': "ClapSelectedSign", "linehl": "ClapSelected"}
let g:clap_current_selection_sign = { 'text': '=>', 'texthl': "ClapCurrentSelectionSign", "linehl": "ClapCurrentSelection"}
let g:clap_enable_background_shadow = v:false

" Pear Tree
let g:pear_tree_ft_disabled = ['clap_input']

" Limelight
let g:limelight_conceal_guifg='#777777'

" Thesaurus
let g:tq_language = ['de', 'en']
let g:tq_map_keys = 0
let g:tq_use_vim_autocompletefunc = 0

" IndentLine
let g:indent_blankline_char = '│'
let g:indent_blankline_use_treesitter = v:true
let g:indent_blankline_show_current_context = v:true
let g:indent_blankline_filetype_exclude = ['help', 'dashboard', 'packer', 'lspinfo', 'clap_input']

" Pandoc
let g:pandoc#modules#disabled = ['folding']
let g:pandoc#after#modules#enabled = ['tablemode']
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#biblio#bibs = [ $HOME . '/iCloud/Documents/Papers/Bibliography.bib']
let g:pandoc#biblio#sources = "gy"
let g:pandoc#filetypes#handled = ["pandoc", "markdown"]
let g:pandoc#spell#enabled = 0
let g:pandoc#spell#default_langs = ['de_de', 'en_gb']
let g:pandoc#formatting#mode = "s"
let g:pandoc#formatting#textwidth = 100
let g:rmd_dynamic_comments = 0
let g:pandoc#formatting#smart_autoformat_blacklist = [
                    \ 'pandoc.+header',
                    \ 'pandoc\S{-}(code|title|line|math)block(title)?',
                    \ 'pdc(delimited|latex)?codeblock',
                    \ 'pandoc.+table',
                    \ 'pandoctable',
                    \ 'pandoc.+latex',
                    \ 'pandocreferencedefinition',
                    \ 'pandocreferencelabel',
                    \ 'tex.*',
                    \ 'pdclatex*',
                    \ 'yaml.*',
                    \ 'pdcyaml',
                    \ 'delimiter',
                    \ 'rmdrChunk'
                    \]
function! s:setSideLabels()
    syn match sideLabels /\%(TODO2:\)/
    hi link sideLabels pandocHeaderID
endfun

augroup pandoc_setup
    autocmd!
    autocmd FileType pandoc setlocal omnifunc=pandoc#completion#Complete
    autocmd FileType pandoc,rmd :call <SID>setSideLabels()
    autocmd BufEnter pandoc,rmd :call <SID>setSideLabels()
    " autocmd bufenter pandoc,rmd autocmd BufWritePre <buffer> call <SID>setSideLabels()
augroup END

" Nvim R
let R_assign_map = "–"
let R_non_r_compl = 0
let R_rconsole_width = 60
let R_min_editor_width = 70
let r_indent_align_args = 0
let rrst_syn_hl_chunk = 1
let R_setwidth = 0
let R_nvimpager = "tab"
let rmd_syn_hl_chunk = 1
let R_nvim_wd = 1
let R_app = "radian"
let R_cmd = "R"
let R_csv_app = 'terminal:vd'
let R_hl_term = 0
let R_args = []
let R_bracketed_paste = 1
let R_close_term = 1
" Use Treesitter for syntax highlighting
let R_hi_fun = 0
" Use RLanguageserver
let R_set_omnifunc = ["rnoweb", "rhelp", "rrst"]
" RStudio like sections
function! s:fillLine( str )
    " set tw to the desired total length
    let tw = &textwidth - 40
    if tw==0 | let tw = 80 | endif
    " strip trailing spaces first
    .s/[[:space:]]*$//
    " calculate total number of 'str's to insert
    let reps = (tw - col("$")) / len(a:str)
    " insert them, if there's room, removing trailing spaces (though forcing there to be one)
    if reps > 0
        .s/$/\=(' '.repeat(a:str, reps))/
    endif
endfunction
augroup r_setup
    autocmd!
    " Fix pipe operator
    autocmd FileType r,rmd inoremap <buffer> <leader>m <Esc><cmd>normal! a %>%<CR>a
    autocmd FileType r,rmd inoremap <buffer> >> <Esc><cmd>normal! a \|><CR>a
    " Sections like RStudio
    autocmd FileType r inoremap <buffer> ## <esc><cmd>call <SID>fillLine( '-' )<CR>o<C-U><CR>
    autocmd FileType r inoremap <buffer> *** <esc><cmd>call <SID>fillLine( '*' )<CR>o<C-U>
    " Repeat # on <Enter>
    autocmd FileType r setlocal formatoptions-=t formatoptions+=croql
    " Use Pandoc bib completion for knitr chunk options
    autocmd FileType rmd set completefunc=pandoc#completion#Complete
augroup END

" Ultest
let g:ultest_pass_sign = ''
let g:ultest_fail_sign = ''
let g:ultest_running_sign = ''
let g:ultest_not_run_sign = ''

" Floaterm
let g:floaterm_title = v:false

" Dashboard
let g:dashboard_default_executive ='clap'
let g:dashboard_custom_section={
            \ 'a_recent_files': {
            \ 'description': ['   Recently Used Files'],
            \ 'command': 'Clap history' },
            \ 'b_find_file': {
            \ 'description': ['   Find File          '],
            \ 'command': 'Clap files' },
            \ 'c_load_session': {
            \ 'description': ['   Load Last Session  '],
            \ 'command': 'SessionLoad' },
            \ 'd_plugins': {
            \ 'description': ['   Plug-ins           '],
            \ 'command': 'tabedit ~/Repositories/dotfiles/nvim/lua/plugins.lua' },
            \ 'e_settings': {
            \ 'description': [' 漣 Settings           '],
            \ 'command': 'tabedit $MYVIMRC' },
            \ }
let g:dashboard_custom_header = [
            \ '       ▐▒▒    ',
            \ '      ▒░▌░▌   ',
            \ '     ░░░▌░█▌  ',
            \ '    ▐░░ █░█▒  ',
            \ '    ░░░░░░█▒▌ ',
            \ '   ▐░░░▀   ▀▒ ',
            \ '   ▒░▀      ▀ ',
            \ '   ▒          ',
            \]
let g:dashboard_custom_footer = ['']
let g:dashboard_session_directory = $HOME . '/iCloud/Vim/session'

augroup dashboard_setup
    autocmd!
    autocmd FileType dashboard setlocal scrolloff=0
augroup END

" Remove all trailing whitespaces
function! s:stripTrailingWhitespaces()
    if !&binary && &filetype != 'diff'
        let l:save = winsaveview()
        keeppatterns %s/\s\+$//e
        call winrestview(l:save)
    endif
endfun
augroup strip_trailing_whitespaces
    autocmd!
    autocmd FileType css,php,r,vim autocmd BufWritePre <buffer> call <SID>stripTrailingWhitespaces()
augroup END

" Vim Rooter
let g:rooter_change_directory_for_non_project_files = 'current'
let g:rooter_patterns = ['.git', 'build.sh']

" ----------------------------------------------------------------------------
" COLORS
" ----------------------------------------------------------------------------

function! s:myHighlightsPaper() abort
    highlight! link pandocHTMLComment Comment
    highlight pandocReferenceLabel gui=bold
    highlight rmdCodeDelim gui=bold
    highlight pandocAtxHeader gui=bold
    highlight pandocCiteKey gui=bold
    highlight pandocUListItemBullet gui=bold
    highlight pandocEmphasis gui=italic
    highlight pandocStrong gui=bold
    highlight pandocStrongEmphasis gui=bold,italic
    highlight pandocEmphasisInStrong gui=bold,italic
    highlight ColorColumn guibg=NONE
    highlight DiagnosticVirtualTextInfo guibg=#F2EEDE guifg=#F2EEDE
endfunction

function! s:myHighlightsEdgeLight() abort
    " Sign column
    highlight SignColumn guibg=#FAFAFA
    " Discreet wrap column
    highlight ColorColumn guibg=#DEE2E7
    " Split column
    highlight VertSplit guibg=#BCC5CF
    " Hide numbers
    highlight LineNr guifg=#FAFAFA
    " Floaterm
    highlight Floaterm guibg=#EEF1F4
    highlight FloatermBorder guibg=#EEF1F4 guifg=#EEF1F4
    " Highlightyank
    highlight HighlightedyankRegion guibg=#61AFEF guifg=#FAFAFA
    " Gitsigns
    highlight DiffAdd guibg=#FAFAFA guifg=#608E32
    highlight DiffChange guibg=#FAFAFA guifg=#BE7E05
    highlight DiffDelete guibg=#FAFAFA guifg=#D05858
    " Ultest
    highlight UltestPass guifg=#608E32
    highlight UltestFail guifg=#D05858
    highlight UltestRunning guifg=#BE7E05
    highlight UltestBorder guifg=#D05858
    highlight UltestSummaryInfo guifg=#5079BE gui=bold
    "LSP Highlight
    highlight LspReferenceRead guibg=#BCC5CF
    highlight LspReferenceText guibg=#BCC5CF
    highlight LspReferenceWrite guibg=#BCC5CF
    " LSP Diagnostics
    highlight DiagnosticSignError guibg=#FAFAFA guifg=#D05858
    highlight DiagnosticSignWarn guibg=#FAFAFA guifg=#BE7E05
    highlight DiagnosticSignInfo guibg=#FAFAFA guifg=#5079BE
    highlight DiagnosticSignHint guibg=#FAFAFA guifg=#608E32
    highlight DiagnosticVirtualTextError guibg=#FAFAFA guifg=#D05858
    highlight DiagnosticVirtualTextWarn guibg=#FAFAFA guifg=#BE7E05
    highlight DiagnosticVirtualTextInfo guibg=#FAFAFA guifg=#5079BE
    highlight DiagnosticVirtualTextHint guibg=#FAFAFA guifg=#608E32
    highlight! LspLightBulbSign guibg=#FAFAFA guifg=#E4D00A

endfunction

function! s:myHighlightsDracula() abort
    " Discreet wrap column
    highlight ColorColumn guibg=#2E3241
    " Hide numbers
    highlight LineNr guifg=#282A36
    " Add dracula syntax highlighting
    highlight pandocEmphasis gui=italic guifg=#F1FA8C
    highlight pandocStrong gui=bold guifg=#FFB86C
    highlight pandocStrongEmphasis gui=bold,italic guifg=#F1FA8C
    highlight pandocEmphasisInStrong gui=bold,italic guifg=#FFB86C
    " Distinguishing terminal mode cursor
    highlight! link TermCursor Cursor
    highlight! TermCursorNC guibg=#FF5555 guifg=#FFFFFF
    " Improve delete colours for Gitsign
    highlight! DiffDelete gui=NONE guifg=#FF5555 guibg=#282A36
    " Floaterm
    highlight Floaterm guibg=#21222C
    highlight FloatermBorder guibg=#21222C guifg=#21222C
    " Ultest
    highlight UltestPass guifg=#50FA7B
    highlight UltestFail guifg=#FF5555
    highlight UltestRunning guifg=#F1FA8C
    highlight UltestBorder guifg=#FF5555
    highlight UltestSummaryInfo guifg=#8BE9FD gui=bold
    " LSP Highlight
    highlight LspReferenceRead guibg=#424450
    highlight LspReferenceText guibg=#424450
    highlight LspReferenceWrite guibg=#424450
    highlight! LspLightBulbSign guibg=#424450 guifg=#F1FA8C
endfunction

augroup my_colours
    autocmd!
    autocmd ColorScheme dracula call <SID>myHighlightsDracula()
    autocmd ColorScheme paper call <SID>myHighlightsPaper()
    autocmd ColorScheme edge call <SID>myHighlightsEdgeLight()
augroup END
" Colour theme
" color dracula
" dark mode enabled?
if system("defaults read -g AppleInterfaceStyle") =~ '^Dark'
   color dracula
else
    color edge
    set background=light
endif
" Use 24bit colours.
set termguicolors
" We'll fake a custom left padding for each window.
set foldcolumn=1

" ----------------------------------------------------------------------------
" KEY MAPS
" ----------------------------------------------------------------------------

" Make it easy to edit the Vimrc file.
nmap <Leader>ev <cmd>tabedit $MYVIMRC<CR>

" Turn off linewise keys. Normally, the `j' and `k' keys move the cursor down one entire line. with
" line wrapping on, this can cause the cursor to actually skip a few lines on the screen because
" it's moving from line N to line N+1 in the file. I want this to act more visually -- I want `down'
" to mean the next line on the screen
nmap j gj
nmap k gk

" Bufonly
nmap <silent> <leader>bd <cmd>BufOnly<CR>

" Zen Mode
nmap <silent> <leader>z <cmd>ZenMode<CR>

" Fuzzy finder
nmap <silent> <leader>f <cmd>Clap files<CR>
nmap <silent> - <cmd>Clap filer %:p:h<CR>
nmap <silent> <leader><leader> <cmd>Clap buffers<CR>
nmap <silent> <leader>s <cmd>Clap grep2 <CR>

" Exit Terminal mode by ESC
tnoremap <Esc> <C-\><C-n>

" Exit Shell program
tnoremap <C-v><Esc> <Esc>

" Thesaurus
nnoremap <leader>ct <cmd>ThesaurusQueryReplaceCurrentWord<CR>
vnoremap <leader>ct y<cmd>ThesaurusQueryReplace <C-r>"<CR>

" Nvim R
vmap ✠ <Plug>RDSendSelection
nmap ✠ <Plug>RDSendLine

" Copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y

" Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" Easy align
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" Incremental search improvements
noremap <silent> n <Cmd>execute('normal! ' . v:count1 . 'n')<CR>
            \<Cmd>lua require('hlslens').start()<CR>
noremap <silent> N <Cmd>execute('normal! ' . v:count1 . 'N')<CR>
            \<Cmd>lua require('hlslens').start()<CR>
noremap * *<Cmd>lua require('hlslens').start()<CR>
noremap # #<Cmd>lua require('hlslens').start()<CR>
noremap g* g*<Cmd>lua require('hlslens').start()<CR>
noremap g# g#<Cmd>lua require('hlslens').start()<CR>
" use : instead of <Cmd>
nnoremap <silent> <leader>l :noh<CR>

" Sayonara
nmap <silent> << <cmd>BufDel<CR>

" Floaterm
noremap  <silent> !! <cmd>FloatermToggle<CR>
noremap! <silent> !! <Esc><cmd>FloatermToggle<CR>
tnoremap <silent> !! <C-\><C-n><cmd>FloatermToggle<CR>

" Undotree
nnoremap <F5> <cmd>MundoToggle<CR>

" Write file
nnoremap <space><space> <cmd>w<CR>

" Dashboard
nmap <leader>ss :<C-u>SessionSave<CR>
nmap <leader>sl :<C-u>SessionLoad<CR>

" Ultest
map <leader>t <cmd>UltestNearest<CR>
map <leader>ta <cmd>Ultest<CR>

map <leader># :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" Spectre
nnoremap <leader>S :lua require('spectre').open()<CR>
