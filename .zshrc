# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

export TERM="xterm-256color"

LANG="de_DE.UTF-8"
LC_COLLATE="de_DE.UTF-8"
LC_CTYPE="de_DE.UTF-8"
LC_MESSAGES="de_DE.UTF-8"
LC_MONETARY="de_DE.UTF-8"
LC_NUMERIC="de_DE.UTF-8"
LC_TIME="de_DE.UTF-8"
LC_ALL="de_DE.UTF-8"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME=""

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    macos
    git
    zsh-completions
    zsh-autosuggestions
    zsh-syntax-highlighting
    history-substring-search
    laravel
)

# Enable completions
autoload -U compinit && compinit

# Enable Autosuggest Async Mode
ZSH_AUTOSUGGEST_USE_ASYNC=true

source $ZSH/oh-my-zsh.sh

# Prompt config
eval "$(starship init zsh)"

# User configuration
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

export GPG_TTY=$(tty)

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

fpath=(/usr/local/share/zsh-completions $fpath)
 
# Fix Ruby OpenSSL bug
#export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# DEVELOPMENT
alias syn='cd ~/Repositories/Synthesise'
alias diss='cd ~/Library/Mobile\ Documents/com~apple~CloudDocs/Dissertation'
alias manuskript='cd ~/Repositories/Manuskript'
alias tsa='cd ~/Repositories/TimeSpaceAnalysis'
alias python='python3'
alias art='php artisan'
alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
alias phpstan="mkdir storage/larastan && php artisan migrate:generate --path=storage/larastan --no-interaction --quiet && ./vendor/bin/phpstan analyse ; rm -r storage/larastan"
alias phpunit='vendor/bin/phpunit'
alias pest='vendor/bin/pest'
alias jigsaw='vendor/bin/jigsaw'
alias pip='pip3'
alias art:test='php artisan test --parallel'
alias art:test:browser='php artisan migrate:fresh --seed --quiet --env=dusk.local && php artisan dusk --without-percy'
alias art:test:visual='php artisan migrate:fresh --seed --quiet --env=dusk.local && php artisan dusk'
alias R="radian"
alias c='clear'
alias skim='open -a Skim.app'

export PATH=$HOME/Repositories/proofreading/ltex-ls-15.2.0/bin:$PATH

alias vim=nvim

alias lemas-apps='autossh -M 0 inventionate@apps.lemas-forschung.de'

alias lemas-cloud='autossh -M 0 inventionate@cloud.lemas-forschung.de'

alias lemas-turn='autossh -M 0 inventionate@turn.lemas-forschung.de'

alias update='sh ~/Repositories/dotfiles/update.sh'

alias rstudio-pres='sh ~/Repositories/dotfiles/rstudio-pres.sh'

alias rstudio-default='sh ~/Repositories/dotfiles/rstudio-default.sh'

export PATH="/usr/local/sbin:$PATH"

export PATH="$HOME/.composer/vendor/bin:$PATH"

export PATH="/Library/Frameworks/R.framework/Versions/Current/Resources:$PATH"

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Vi mode
bindkey -v

bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward
#bindkey '^ ' forward-word
bindkey '^[f' forward-word
bindkey "^[b" backward-word

export KEYTIMEOUT=1

# EXA

# general use
alias ls='exa'                                                         # ls
alias l='exa -lbF --git'                                               # list, size, type, git
alias ll='exa -lbGF --git'                                             # long list
alias llm='exa -lbGF --git --sort=modified'                            # long list, modified date sort
alias la='exa -lbhHigUmuSa --time-style=long-iso --git --color-scale'  # all list
alias lx='exa -lbhHigUmuSa@ --time-style=long-iso --git --color-scale' # all + extended list

# speciality views
alias lS='exa -1'                                                       # one column, just names
alias lt='exa --tree --level=2'                                         # tree

# Proofreading
alias proofreading='sh $HOME/Repositories/proofreading/proofreading.sh'

# Media keys
alias mp='music play'
alias mn='music next'
alias mb='music previous'
alias ms='music pause'

# Xaringan
alias inf_mr="RScript -e 'xaringan::inf_mr(\"slides.Rmd\")'"
alias build_mr="RScript -e 'rmarkdown::render(\"slides.Rmd\")'"
alias open_mr="open http://127.0.0.1:4321/slides.html#1"
alias export_pdf="Rscript -e 'pagedown::chrome_print(\"slides.html\", options = list(transferMode = \"ReturnAsStream\"))'"

# R
alias build_r_package="/Library/Frameworks/R.framework/Versions/Current/Resources/R CMD INSTALL --no-multiarch --with-keep.source ."

alias build_r_package_clean="/Library/Frameworks/R.framework/Versions/Current/Resources/R CMD INSTALL --preclean --no-multiarch --with-keep.source ."

# Shrink PDF
alias shrink_pdf="sh $HOME/Repositories/dotfiles/shrinkpdf.sh"

# Certbot
function certbot-renew() {
    sudo certbot certonly --manual -d www.$1 -d $1 --agree-tos --manual-public-ip-logging-ok --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory --register-unsafely-without-email --rsa-key-size 4096
    sudo cp /etc/letsencrypt/live/www.$1/fullchain.pem $HOME/Desktop/fullchain.pem
    sudo cp /etc/letsencrypt/live/www.$1/privkey.pem $HOME/Desktop/privkey.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/fullchain.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/privkey.pem
}
