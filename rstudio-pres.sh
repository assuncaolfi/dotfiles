#!/bin/zsh

# Enable RStudio presenting mode

mv ~/.Rprofile ~/.Rprofile2

rm ~/.config/rstudio/rstudio-prefs.json

ln -s ~/Repositories/dotfiles/rstudio-prefs-pres.json ~/.config/rstudio/rstudio-prefs.json
