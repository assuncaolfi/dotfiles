if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  augroup InstallPlugins
      autocmd!
      autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  augroup END
endif

" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" ----------------------------------------------------------------------------
" GENERAL
" ----------------------------------------------------------------------------

Plug 'dracula/vim', { 'as': 'dracula' }                             " Colour scheme
Plug '~/.fzf'                                                       " Fuzzy file search
Plug 'junegunn/fzf.vim'                                             " Fuzzy file search
Plug 'tpope/vim-surround'                                           " Edit surroundings
Plug 'tpope/vim-repeat'                                             " Extended repeat support
Plug 'tpope/vim-vinegar'                                            " Better file commander
Plug 'tpope/vim-fugitive'                                           " Git integration
Plug 'tpope/vim-eunuch'                                             " Better terminal commands
Plug 'tpope/vim-ragtag'                                             " Autoclose and edit tags
Plug 'tpope/vim-abolish'                                            " Better search and replace
Plug 'tpope/vim-sleuth'                                             " Auto format files
Plug 'scrooloose/nerdcommenter'                                     " Pretty commenter
Plug 'kana/vim-smartinput'                                          " Autoclose quotes, brackets, etc.
Plug 'kshenoy/vim-signature'                                        " Better marks support
Plug 'SirVer/ultisnips'                                             " Snippet supprt
Plug 'honza/vim-snippets'                                           " Snippet library
Plug 'mhinz/vim-signify'                                            " Show git info in left column
Plug 'ChesleyTan/wordCount.vim'                                     " Count written words
Plug 'mhinz/vim-startify'                                           " Better startscreen
Plug 'chrisbra/csv.vim', { 'for': 'csv' }                           " Better CSV display
Plug 'sheerun/vim-polyglot'                                         " Better language support (syntax, filetype, etc.)
Plug 'haya14busa/is.vim'                                            " Better search
Plug 'osyo-manga/vim-anzu'                                          " Count search
Plug 'w0rp/ale'                                                     " Live syntax validation and autoformat
Plug 'itchyny/lightline.vim'                                        " Better info bar
Plug 'maximbaz/lightline-ale'                                       " Lightline ALE support
Plug 'editorconfig/editorconfig-vim'                                " Respect project editor configs
Plug 'junegunn/vim-easy-align', {'on': '<plug>(LiveEasyAlign)'}     " Easy align
Plug 'terryma/vim-multiple-cursors'                                 " Multiple cursors
Plug 'ryanoasis/vim-devicons'                                       " File icons
Plug 'easymotion/vim-easymotion'                                    " Better navigation
Plug 'nelstrom/vim-visual-star-search'                              " Visual search
Plug 'machakann/vim-highlightedyank'                                " Highlight yank
Plug 'mhinz/vim-tree'                                               " Tree navigation

" Initialize plugin system
call plug#end()
